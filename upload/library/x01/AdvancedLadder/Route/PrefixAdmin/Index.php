<?php

class x01_AdvancedLadder_Route_PrefixAdmin_Index implements XenForo_Route_Interface
{
	public function match($routePath, Zend_Controller_Request_Http $request, XenForo_Router $router)
	{
            $action = $router->resolveActionWithIntegerParam($routePath, $request, 'user_group_id');
		return $router->getRouteMatch('x01_AdvancedLadder_ControllerAdmin_Index', $action, 'x01_advanced_user_ladder_adminIndex');
	}
}