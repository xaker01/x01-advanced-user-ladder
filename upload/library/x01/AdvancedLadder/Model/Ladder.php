<?php

class x01_AdvancedLadder_Model_Ladder extends XenForo_Model
{

    public function __construct()
    {
        $this->db = XenForo_Application::get('db');
    }

    public function getLadders()
    {
        $ladders = $this->db->fetchAll("SELECT * FROM `xf_x01AdvancedLadder_ladder` ");
        return $ladders;
    }

    public function getLaddersBygroupid($group_id)
    {
        $ladders = $this->db->fetchAll("SELECT * FROM `xf_x01AdvancedLadder_ladder` WHERE groups='" . $group_id . "' ");
        return $ladders;
    }

    /*
     * Получить код звания по id пользователя
     */

    public function getLadderByuserid($user_id)
    {
        $ladder = false;
        $ladder_id = $this->db->fetchOne("SELECT ladder_id FROM `xf_x01AdvancedLadder_userladder` WHERE user_id='" . $user_id . "' ");
        if ($ladder_id) {
            $ladder = $this->db->fetchOne("SELECT viewer FROM `xf_x01AdvancedLadder_ladder` WHERE ladder_id='" . $ladder_id . "' ");
        }
        return $ladder;
    }

    public function deleteLadder($levels)
    {
        if (!$levels) {
            return;
        }
        var_dump($levels);
        $this->db->delete('xf_x01AdvancedLadder_ladder', 'ladder_id IN (' . $this->db->quote($levels) . ')');
    }

    public function updateLadder($update)
    {
        foreach ($update as $key => $value) {
            $ladders = $this->db->update('xf_x01AdvancedLadder_ladder', $value, 'ladder_id=' . $value['ladder_id']);
        }
    }

    /*
     * Добавляем в базу наши звания по группе
     */

    public function insertLadder($title, $ball, $messages, $likes, $group, $weight)
    {
        $ball = intval($ball);
        $messages = intval($messages);
        $likes = intval($likes);

        if ($messages < 0) {
            $messages = 0;
        }

        if ($likes < 0) {
            $likes = 0;
        }

        $this->_getDb()->insert('xf_x01AdvancedLadder_ladder', array(
            'balls' => $ball,
            'messages' => $messages,
            'likes' => $likes,
            'groups' => $group,
            'weight' => $weight,
            'viewer' => utf8_substr($title, 0, 250)
        ));
    }

    public function setLadderUser($user_id, $ladder_id)
    {
        if (!$this->getLadderByuserid($user_id)) {
            $this->db->insert('xf_x01AdvancedLadder_userladder', array(
                'user_id' => $user_id,
                'ladder_id' => $ladder_id
            ));
        } else {
            $ladders = $this->db->update('xf_x01AdvancedLadder_userladder', array('ladder_id' => $ladder_id), 'user_id=' . $user_id);
        }
    }

    /**
     *
     */
    public static function cronall()
    {
        $userModel = XenForo_Model::create('XenForo_Model_User');
        $thismodel = XenForo_Model::create('x01_AdvancedLadder_Model_Ladder');

        $users = $userModel->getUsers(array(
            'last_activity' => array('>', XenForo_Application::$time - 4320 * 3600)
        ), array(
            'join' => XenForo_Model_User::FETCH_USER_FULL
        ));

        if (isset($users)) {
            foreach ($users as $user) {
                $ladders = $thismodel->getLaddersBygroupid($user['user_group_id']);
                $setladders[] = array();
                if (sizeof($ladders)) {

                    foreach ($ladders as $ladder) {
                        if (((int)$user['message_count'] >= (int)$ladder['messages']) and ((int)$user['like_count'] >= (int)$ladder['likes']) and ((int)$user['trophy_points'] >= (int)$ladder['balls'])) {

                            $setladders[] = array('ladder_id' => $ladder['ladder_id'],
                                'weight' => $ladder['weight']);
                        }
                    }

                    if (isset($setladders[1])) {

                        foreach ($setladders as $setladder) {
                            if (isset($setladder['weight'])) {
                                $weight[] = $setladder['weight'];
                            } else {
                                $weight[] = '0';
                            }
                        }

                        $keys = array_search(max($weight), $weight);
                        $newladder = $setladders[$keys]['ladder_id'];
                    }


                    if (@sizeof($newladder)) {
                        $thismodel->setLadderUser($user['user_id'], $newladder);
                    }

                }
                $setladders=NULL;
                $weight=NULL;

            }
        }

    }
}