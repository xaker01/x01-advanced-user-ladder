<?php

class x01_AdvancedLadder_Installer {

    protected static $_tables = array(
        'ladders' => array(
            'createQuery' => 'CREATE TABLE IF NOT EXISTS `xf_x01AdvancedLadder_ladder` (
				`ladder_id` INT(10) UNSIGNED AUTO_INCREMENT
				,`viewer` TEXT
				,`groups` INT(10) UNSIGNED NOT NULL DEFAULT \'0\'
				,`balls` INT(10) UNSIGNED NOT NULL DEFAULT \'0\'
				,`messages` INT(10) UNSIGNED NOT NULL DEFAULT \'0\'
				,`likes` INT(10) UNSIGNED NOT NULL DEFAULT \'0\'
                                ,`weight` INT(10) UNSIGNED NOT NULL DEFAULT \'1\'
				, PRIMARY KEY (`ladder_id`)
				
			) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;',
            'dropQuery' => 'DROP TABLE IF EXISTS `xf_x01AdvancedLadder_ladder`',
        ),
        'user_ladder' => array(
            'createQuery' => 'CREATE TABLE IF NOT EXISTS `xf_x01AdvancedLadder_userladder` (
				`userladder_id` INT(10) UNSIGNED AUTO_INCREMENT
				,`user_id` INT(10) UNSIGNED NOT NULL
				,`ladder_id` INT(10) UNSIGNED NOT NULL DEFAULT \'0\'
				, PRIMARY KEY (`userladder_id`)
			) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;',
            'dropQuery' => 'DROP TABLE IF EXISTS `xf_x01AdvancedLadder_userladder`',
        ),
    );

    public static function install($existingAddOn, $addOnData) {
        $db = XenForo_Application::get('db');

        foreach (self::$_tables as $table) {
            $db->query($table['createQuery']);
        }
        $db->insert('xf_x01AdvancedLadder_ladder', array(
            'balls' => '1',
            'messages' => '1',
            'likes' => '1',
            'groups' => '1',
            'weight' => '1',
            'viewer' => utf8_substr('Аномальный гость', 0, 250)
        ));
        $db->insert('xf_x01AdvancedLadder_userladder', array(
            'user_id' => '0',
            'ladder_id' => '0'
        ));
    }

}
