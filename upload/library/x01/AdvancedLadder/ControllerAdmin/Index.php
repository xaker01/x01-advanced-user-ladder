<?php
class x01_AdvancedLadder_ControllerAdmin_Index extends XenForo_ControllerAdmin_Abstract
{
	
	public function actionIndex()
	{
		
		$viewParams = array(
			'userGroups'                    => $this->_getUserGroupModel()->getAllUserGroups()
                );

		return $this->responseView('', 'x01_advanced_user_ladder_admin', $viewParams);
	}
        
        public function actionEdit()
	{
		$userGroupId = $this->_input->filterSingle('user_group_id', XenForo_Input::UINT);
		$userGroup = $this->_getUserGroupOrError($userGroupId);
               $trophyModel = $this->_getLadderModel();
               $ladders=$trophyModel->getLaddersBygroupid($userGroup['user_group_id']);
                $viewParams = array(
		'GroupTitle' => $userGroup['title'],
        'GroupId' => $userGroup['user_group_id'],
                    'titles'=> $trophyModel->getLaddersBygroupid($userGroup['user_group_id'])
        );
                return $this->responseView('', 'x01_advanced_user_ladder_edit', $viewParams);
	}
        
        /**
	 * Updates existing titles, deletes specified ones, and optionally creates
	 * a new one.
	 *
	 * @return XenForo_ControllerResponse_Abstract
	 */
	public function actionUpdate()
	{
		$this->_assertPostOnly();
		//получаем то что нужно обновить
		$update = $this->_input->filterSingle('update', XenForo_Input::ARRAY_SIMPLE);
		//получаем то что нужно удалить
		$delete = $this->_input->filterSingle('delete', array(XenForo_Input::UINT, 'array' => true));
		foreach ($delete AS $deletePoint)
		{
			unset($update[$deletePoint]);
		}
                //TODO
		$input = $this->_input->filter(array(
		'minimum_balls' => XenForo_Input::UINT,
		'minimum_mesagges' => XenForo_Input::UINT,
		'minimum_like' => XenForo_Input::UINT,
		'title' => XenForo_Input::STRING,
		'groupid' => XenForo_Input::UINT,
                'weight'=>XenForo_Input::UINT
		));

		$trophyModel = $this->_getLadderModel();

		//удаляем
		$trophyModel->deleteLadder($delete);
		//обновляем
		$trophyModel->updateLadder($update);

		//если есть новые значения то добавляем
		if ($input['title'])
		{
			$trophyModel->insertLadder($input['title'],$input['minimum_balls'],$input['minimum_mesagges'], $input['minimum_like'],$input['groupid'],$input['weight']);
		}

		//TODO кеширование
		return $this->responseRedirect(
			XenForo_ControllerResponse_Redirect::SUCCESS,
			XenForo_Link::buildAdminLink('advanced-ladder')
		);
	}

        
        
        protected function _getLadderModel()
	{
		return $this->getModelFromCache('x01_AdvancedLadder_Model_Ladder');
	}
        
        /**
	 * Gets the user group model.
	 *
	 * @return XenForo_Model_UserGroup
	 */
	protected function _getUserGroupModel()
	{
		return $this->getModelFromCache('XenForo_Model_UserGroup');
	}
        /**
	 * Gets the specified user group or throws an error.
	 *
	 * @param integer $userGroupId
	 *
	 * @return array
	 */
	protected function _getUserGroupOrError($userGroupId)
	{
		$userGroup = $this->_getUserGroupModel()->getUserGroupById($userGroupId);
		if (!$userGroup)
		{
			throw $this->responseException($this->responseError(new XenForo_Phrase('requested_user_group_not_found'), 404));
		}

		return $userGroup;
	}
}
